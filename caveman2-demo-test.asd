(defsystem "caveman2-demo-test"
  :defsystem-depends-on ("prove-asdf")
  :author "Sammi Johnson (she/her)"
  :license "AGPLv3 or later"
  :depends-on ("caveman2-demo"
               "prove")
  :components ((:module "tests"
                :components
                ((:test-file "caveman2-demo"))))
  :description "Test system for caveman2-demo"
  :perform (test-op (op c) (symbol-call :prove-asdf :run-test-system c)))
